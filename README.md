# Gherkin linter running in docker

## Running linter

`docker run --rm --name gherkin-lint-docker -v $PWD:/gherkin-lint/data gherkin-lint-docker`

## Description

This docker image is just [vsiakka/gherkin-lint](https://github.com/vsiakka/gherkin-lint) in docker container for convinience.

I use official node image based on Alpine Linux to minimize image size.

⚠ It is not stable solution! Use it on your own risk!

There is no any autopush system in here yet.

## Source codes

Located [here](https://gitlab.com/nett00n/gherkin-lint-docker).

## Dockerhub

[Link](https://hub.docker.com/repository/docker/nett00n/gherkin-lint-docker)

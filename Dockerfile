FROM node:alpine
WORKDIR /gherkin-lint/
COPY ./gherkin-litrc.default /gherkin-lint/
RUN npm install gherkin-lint@4.2.1 && \
  mkdir -p /gherkin-lint/rules /gherkin-lint/data
ENTRYPOINT find /gherkin-lint/rules/.gherkin-lintrc 2>/dev/null 1>/dev/null || cp /gherkin-lint/gherkin-litrc.default /gherkin-lint/rules/.gherkin-litrc ;\
  /gherkin-lint/node_modules/.bin/gherkin-lint -c /gherkin-lint/rules/.gherkin-litrc /gherkin-lint/data/
